from django.shortcuts import render, get_object_or_404, redirect
from .models import Usuario
from .forms import UsuarioForm

# Create your views here.
def usuario_listar(request):
    listar = Usuario.objects.all()
    return render(request, 'crudusuario/usuario_listar.html', {'listar': listar})

def usuario_detalle(request, pk):
    detalle = get_object_or_404(Usuario, pk=pk)
    return render(request, 'crudusuario/usuario_detalle.html', {'detalle': detalle})

def usuario_nuevo(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = UsuarioForm()
    return render(request, 'crudusuario/usuario_nuevo.html', {'form': form})

def usuario_editar(request, pk):
    usuario = get_object_or_404(Usuario, pk=pk)
    if request.method == "POST":
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            return redirect('usuario_detalle', pk=usuario.pk)
    else:
        form = UsuarioForm(instance=usuario)
    return render(request, 'crudusuario/usuario_nuevo.html', {'form': form})

def usuario_eliminar(request, pk):
    usuario = get_object_or_404(Usuario, pk=pk)
    usuario.delete()
    return redirect('/')