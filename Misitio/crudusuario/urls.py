from django.urls import path
from . import views

urlpatterns = [
    path('', views.usuario_listar, name='usuario_listar'),
    path('usuario/<int:pk>/', views.usuario_detalle, name='usuario_detalle'),
    path('usuario/nuevo', views.usuario_nuevo, name='usuario_nuevo'),
    path('usuario/<int:pk>/editar/', views.usuario_editar, name='usuario_editar'),
    path('usuario/<pk>/eliminar/', views.usuario_eliminar, name='usuario_eliminar'),
]