from django.db import models

# Create your models here.
class Usuario(models.Model):
    nombre = models.CharField(max_length=50)
    direccion = models.TextField()
    correo = models.EmailField(unique=True)
    usuario = models.CharField(max_length=15, help_text="El nombre de usuario debe ser entre 2 y 15 caracteres")
    contrasena = models.CharField(max_length=50)
    re_contrasena = models.CharField(max_length=50)

    def Crear(self):
        self.save()

    def __str__(self):
        return self.usuario
